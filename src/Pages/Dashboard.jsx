import React, {useMemo, useState } from 'react'
import { useParams } from 'react-router-dom'
import TableComponent from '../components/table.component';
import { users } from '../data';

export default function Dashboard() {
    const {id} = useParams();
    const [selectedValue, setSelectedValue] = useState()
    
   
    const MeanCalculation =(value) =>{
        const sum = value.reduce((acc, curr) => acc + curr, 0);
        return (sum / value.length).toFixed(2);
    }
    const MedianCacluation = (value) =>{
       
       value.sort((a, b) => a - b);
        const length = value.length;
        const middle = Math.floor(length / 2);
    
        if (length % 2 === 0) {
            return (value[middle - 1] + value[middle]) / 2;
        } else {
            return value[middle];
        }
    }

    const ModeCalculation = (value) =>{
        const frequencyMap = {};
        let maxFrequency = 0;
        let modes = [];
    
     value.forEach(number => {
            frequencyMap[number] = (frequencyMap[number] || 0) + 1;
            if (frequencyMap[number] > maxFrequency) {
                maxFrequency = frequencyMap[number];
                modes = [number];
            } else if (frequencyMap[number] === maxFrequency) {
                modes.push(number);
            }
        });
    
        if (modes.length === Object.keys(frequencyMap).length) {
            // If all numbers appear with the same frequency, there is no mode
            return "No mode";
        }
    
        return modes;
    }

    const mean = useMemo(() => selectedValue && MeanCalculation(selectedValue), [selectedValue]);
    const median = useMemo(() => selectedValue && MedianCacluation(selectedValue), [selectedValue]);
    const mode = useMemo(() => selectedValue && ModeCalculation(selectedValue), [selectedValue]);
    
  return (
    <div>
        <TableComponent data={users} id={id} rowData={(value) => setSelectedValue(value)}/>
       {selectedValue && <div className='tableSectionMain'>
            <div><p><b>Mean :</b></p><p>{mean}</p></div>
            <div><p><b>Mode :</b></p> <p>{mode}</p></div>
            <div><p><b>Median :</b></p>{median}<p></p></div>
        </div>}
    </div>
  )
}
