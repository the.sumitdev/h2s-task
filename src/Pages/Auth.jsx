import React, { useEffect, useState } from 'react'
import { Tab, Tabs } from "react-bootstrap";
import Login from '../components/login.component';
import { useLocation, useNavigate } from 'react-router-dom';
export default function Auth() {
    const {pathname} = useLocation();
    const navigate = useNavigate()
    const [key, setKey] = useState();
    // // useEffect(() =>{
    // //     const newPathname = pathname.slice(1);
    // //     newPathname.trim() ? setKey(newPathname) : setKey('login');

    // // },[pathname])
    // const handleTabs = (k) => {
    //     navigate(`/${k}`)
        
    // }    
  return (
    <div className="auth-wrapper">
          <div className="auth-inner">
          <Tabs
              id="controlled-tab-example"
              activeKey={'login'}
            //   onSelect={(k) => handleTabs(k)}
              className="mb-3"
            >
                <Tab eventKey="login" title="Log In">
                <Login/>
                
              </Tab>
              
                </Tabs>
    
    </div>
    </div>
  )
}
