import React from 'react'
import Table from 'react-bootstrap/Table';
import './table.css'
export default function TableComponent({data, id, rowData}) {
    
    const handleRowClick =(value) =>{
        const array = data.map(user => user[value]);
        console.log(array)
        rowData(array)
    }
  return (

   <Table striped responsive>
    <thead>
        <tr>
          <th>#</th>
          <th>Name</th>
          <th onClick={() => handleRowClick('age')}>Age</th>
          <th onClick={() => handleRowClick('score')}>Score</th>
        </tr>
      </thead>
      <tbody>
       
        {
            data && data.map((item) =>{
                return(
                    <tr key={item.id} className={item.id == id ? 'active':''}>
                    <td>
                        {item.id}
                    </td>
                    <td>
                        {item.name}
                    </td>
                    <td>
                        {item.age}
                    </td>
                    <td>
                        {item.score}
                    </td>
                    </tr>
                )
            })
        }
       
      </tbody>
   </Table>
  )
}
