import React, { useState } from 'react'
import { Link, useNavigate } from 'react-router-dom'
import { users } from '../data';

export default function Login() {
    const [formData, setFormData] = useState({
        name: ''
      });
      const navigate = useNavigate()
    
      const [formErrors, setFormErrors] = useState({});
    
      const handleChange = (e) => {
        const { name, value } = e.target;
        setFormData({
          ...formData,
          [name]: value,
        });
      };

      const handleSubmit = (e) => {
        e.preventDefault();
        const isValid = validateForm();
        if (isValid) {
           const user = findUser(formData.name)
            if(user){
                navigate(`/${user.id}`)
            }
        
        }
      };

    const findUser = (name) =>{
       return users.find(item => item.name.toLowerCase() == name.toLowerCase()) // for get user by user name
    }


    const validateForm = () => {
        const errors = {};
         
        if(!formData.name.trim()){
            errors.name = 'Name is required';
        }else{
           
             if(!findUser(formData.name)){
            errors.name = 'Invalid name';
        }
    }
    
        setFormErrors(errors);
        return Object.keys(errors).length === 0; // Return true if there are no errors
      };
  return (
    <form onSubmit={handleSubmit}>
        {/* <h3>Login In</h3> */}

        <div className="mb-3">
      <label>Name</label>
      <input
        type="text"
        name="name"
        className="form-control"
        placeholder="Name"
        value={formData.name}
        onChange={handleChange}
      />
      {formErrors.name && <p className="error">{formErrors.name}</p>}
    </div>
    <div className="d-grid">
    <button type="submit" className="btn btn-primary">Log In</button>
    </div>
    </form>
    
  )
}
