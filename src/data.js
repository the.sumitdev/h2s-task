export const users=[
    {
        id: 1,
        name: 'John Doe',
        age: 28,
        score: 85
    },
    {
        id: 2,
        name: 'Jane Smith',
        age: 32,
        score: 92
    },
    {
        id: 3,
        name: 'Sumit',
        age: 27,
        score: 99
    },
    {
        id: 4,
        name: 'Rohit Kumar',
        age: 24,
        score: 90
    },
    {
        id: 5,
        name: 'Shubham',
        age: 25,
        score: 89
    },
    {
        id: 6,
        name: 'Amit Kumar',
        age: 29,
        score: 65
    },
    {
        id: 7,
        name: 'Umesh Rautela',
        age: 22,
        score: 80
    },
    {
        id: 8,
        name: 'Hitesh',
        age: 18,
        score: 97
    },
    {
        id: 9,
        name: 'Sahil',
        age: 21,
        score: 96
    },
]