// import logo from './logo.svg';
import './App.css';
import { Link, Route, BrowserRouter as Router, Routes, useLocation, useNavigate } from 'react-router-dom';
import Login from './components/login.component';

import Auth from './Pages/Auth';
import { useEffect } from 'react';
import Dashboard from './Pages/Dashboard';

function App() {


  return (
    <Router>
      <div className="App">
        <nav className="navbar navbar-expand-lg navbar-light fixed-top">
          <div className="container">
            <Link className="navbar-brand logo" to='/'>
             <h3>H2S<span>TASK</span></h3>
            </Link>
            <div className="collapse navbar-collapse" id="navbarTogglerDemo02">
              <ul className="navbar-nav ml-auto">
              </ul>
            </div>
          </div>
        </nav>
        <div className="height"></div>

        
            <Routes>
              <Route exact path="/" element={<Auth />} />
              <Route exact path="/:id" element={<Dashboard />} />
                          
            </Routes>
         
      </div>
    </Router>
  
  );
}

export default App;
